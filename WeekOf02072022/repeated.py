def repeated(str):
  for i in range(1, len(str)):
    substrng = str[0:i+1]
    if str.count(substrng) == len(str) / len(substrng) and str != substrng:
      return True
  return False
