function getVowelSubstrings(str) {
  str = str.split('')
  let vowels = ['a', 'e', 'i', 'o', 'u']
  let o = []
  for(let i = 0; i < str.length; i++) {
    if(vowels.includes(str[i].toLowerCase())) {
      for(let j = i; j < str.length; j++) {
        if(!o.includes(str.join('').substring(i,j+1)) && vowels.includes(str[j].toLowerCase())) {
          o.push(str.join('').substring(i,j+1))
        }
      }
    }
  }
  o.sort()
  return o
}

function getConsonantSubstrings(str) {
  str = str.split('')
  let vowels = ['a', 'e', 'i', 'o', 'u']
  let o = []
  for(let i = 0; i < str.length; i++) {
    if(!vowels.includes(str[i].toLowerCase())) {
      for(let j = i; j < str.length; j++) {
        if(!o.includes(str.join('').substring(i, j+1)) && !vowels.includes(str[j].toLowerCase())) {
          o.push(str.join('').substring(i, j+1))
        }
      }
    }
  }
  o.sort()
  return o
}

function redundant(str) {
  return () => { return str }
}
