function additivePersistence(n) {
    let i = 0
    while(n > 9) {
        i++
        let s = 0
        n = n.toString().split('')
        n.forEach(e => { s += parseInt(e) })
        n = s
    }
    return i
}

function multiplicativePersistence(n) {
    let i = 0
    while(n > 9) {
        i++
        let m = 1
        n = n.toString().split('')
        n.forEach(e => { m *= parseInt(e) })
        n = m
    }
    return i
}
