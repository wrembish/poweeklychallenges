def can_see_stage(seats):
    i = 0
    for j in range(0, len(seats[0])):
        maxH = -1
        for i in range(0, len(seats)):
            if seats[i][j] > maxH:
                maxH = seats[i][j]
            else:
                return False
    return True
