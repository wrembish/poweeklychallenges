function multiplyBy11(nStr){
  return nStr * "11"
}

function filterArray(arr) {
  return arr.filter(e => { return typeof e != 'string' })
}


// logical reasoning question answer:
// switch on 2 of the switches and give it time for the lightbulbs to heat up
// then switch off one of the bulbs and go check the lighs
// say you switched on 1 and 2 and let them heat up then turned off 1
// 1 will be off and hot, 2 will be on, and 3 will be off and cold
