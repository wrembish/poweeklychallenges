function isDisarium(n) {
  let tot = 0
  n.toString().split('').forEach((val, i) => { tot += parseInt(Math.pow(val, i+1)) })

  return n === tot
}
